// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'audio.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AudioObservable on AudioBase, Store {
  final _$valueAtom = Atom(name: 'AudioBase.value');

  @override
  List<String> get value {
    _$valueAtom.reportRead();
    return super.value;
  }

  @override
  set value(List<String> value) {
    _$valueAtom.reportWrite(value, super.value, () {
      super.value = value;
    });
  }

  final _$uidAtom = Atom(name: 'AudioBase.uid');

  @override
  int get uid {
    _$uidAtom.reportRead();
    return super.uid;
  }

  @override
  set uid(int value) {
    _$uidAtom.reportWrite(value, super.uid, () {
      super.uid = value;
    });
  }

  final _$AudioBaseActionController = ActionController(name: 'AudioBase');

  @override
  void register(String path) {
    final _$actionInfo =
        _$AudioBaseActionController.startAction(name: 'AudioBase.register');
    try {
      return super.register(path);
    } finally {
      _$AudioBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void remove(String path) {
    final _$actionInfo =
        _$AudioBaseActionController.startAction(name: 'AudioBase.remove');
    try {
      return super.remove(path);
    } finally {
      _$AudioBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
value: ${value},
uid: ${uid}
    ''';
  }
}
