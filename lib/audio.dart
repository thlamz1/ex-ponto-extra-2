import 'package:mobx/mobx.dart';
part 'audio.g.dart';

class AudioObservable = AudioBase with _$AudioObservable;

abstract class AudioBase with Store {
  @observable
  List<String> value = [];

  @observable
  int uid = 0;

  @action
  void register(String path) {
    value.add(path);
    uid++;
  }

  @action
  void remove(String path) {
    value.remove(path);
  }
}
